package com.csg.service;

import java.util.Map;

public interface UserQuestionsCall 
{
	public String userQuestionsCall(Map<String, Map<String, String>> ExternalMap, String sessionId, String action, String questionsId);

}
