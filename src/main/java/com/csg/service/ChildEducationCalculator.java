package com.csg.service;

public interface ChildEducationCalculator 
{
	public String childEducationCalculator(String age, String graduation, String studyType, String Country, String currentSavings, 
			String childAge,String inflationRate, String rateReturnInvestment);

}
