package com.csg.service;

public interface RegularSavingsCalc 
{
	public String regularSavingCalculation(String age, String goalAge, String currentCostOfGoal, String currentProvision,
			String inflationRate, String rateReturnInvestment);
}
