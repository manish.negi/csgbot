package com.csg.service;

import java.util.Map;

public interface CSGQuestionsCall 
{
	public String csgQuestionsServiceCall(Map<String,Map<String,String>> ExternalMap, String sessionId, String action);

}
