package com.csg.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class APIResponse
{
	String speech="";
	public String apiResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  ],	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"facebook\": {	");
		sb.append("	      \"type\": \"Chatbot\",	");
		sb.append("	      \"platform\": \"API.AI\",	");
		sb.append("	      \"title\": \"MLIChatBot\",	");
		sb.append("	      \"imageUrl\": \"BOT\",	");
		sb.append("	      \"buttons\": [	");

		StringBuffer sb2= new StringBuffer();
		Map<String, String> newMap = ExternalMap.get(sessionId);
		int size = newMap.size();
		int count=0;
		for (Map.Entry<String, String> entry : newMap.entrySet())
		{
			count++;
			String key = entry.getKey();
			String value = entry.getValue();

			sb2.append("	        {	");
			sb2.append("	          \"text\": \" "+value+"\",	");
			sb2.append("	          \"postback\": \""+key+"\",	");
			sb2.append("	          \"link\": \"\"	");
			if(count!=size)
			{
				sb2.append("	        },	");
			}
			else
			{
				sb2.append("	        }	");
			}
		}
		sb.append(sb2.toString());

		StringBuffer sb3 = new StringBuffer();
		sb3.append("	      ]	");
		sb3.append("	    }	");
		sb3.append("	  },	");
		sb3.append("	  \"source\": \"java-webhook\"	");
		sb3.append("	}	");
		sb.append(sb3.toString());
		System.out.println(sb.toString());
		return sb.toString();
	}
}
