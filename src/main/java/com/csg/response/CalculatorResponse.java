package com.csg.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class CalculatorResponse 
{
	String speech="";
	public String calculatorResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action)
	{
		speech="Thank you for sharing the information with us.Assuming the inflation rate at 6% and Rate of return of investment"
				+ "at 8%, following is the break up of your financial needs-";
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  ],	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"facebook\": {	");
		sb.append("	      \"type\": \"Chatbot\",	");
		sb.append("	      \"platform\": \"API.AI\",	");
		sb.append("	      \"title\": \"MLIChatBot\",	");
		sb.append("	      \"imageUrl\": \"BOT\",	");
		sb.append("	      \"buttons\": [	");

		StringBuffer sb2= new StringBuffer();
		Map<String, String> newMap = ExternalMap.get(sessionId);
		String str1=newMap.get("protectionofmyfamily")+"";
		int var=0;
		if("null".equalsIgnoreCase(str1))
		{
			str1="";
			
		}
		else {
			var++;
		}
		String str2=newMap.get("RegularSaving")+"";
		if("null".equalsIgnoreCase(str2))
		{
			str2="";
		}
		else {
			var++;
		}
		String str3=newMap.get("Retirement")+"";
		if("null".equalsIgnoreCase(str3))
		{
			str3="";
		}
		else {
			var++;
		}
		String str4=newMap.get("ChildEducation")+"";
		if("null".equalsIgnoreCase(str4))
		{
			str4="";
		}
		else {
			var++;
		}
		String str5=newMap.get("ChildMarriage")+"";
		if("null".equalsIgnoreCase(str5))
		{
			str5="";
		}
		else {
			var++;
		}
		String plan=str1+"#"+str2+"#"+str3+"#"+str4+"#"+str5;
		String [] planSplit = plan.split("#");
		int size= planSplit.length;
		int count=1;
		for (Map.Entry<String, String> entry : newMap.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();
			if("protectionofmyfamily".equalsIgnoreCase(key) || "RegularSaving".equalsIgnoreCase(key)
					||"Retirement".equalsIgnoreCase(key)||"ChildEducation".equalsIgnoreCase(key)||"ChildMarriage".equalsIgnoreCase(key))
			{
				count++;
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+key+"\",	");
				sb2.append("	          \"postback\": \""+value+"\",	");
				sb2.append("	          \"link\": \"\"	");
				if(var!=1)
				{
					if(count!=size)
					{
						sb2.append("	        },	");
					}
					else
					{
						sb2.append("	        }	");
					}	
				}
				else
				{
					sb2.append("	        }	");
				}	
			}
		}
		sb.append(sb2.toString());

		StringBuffer sb3 = new StringBuffer();
		sb3.append("	      ]	");
		sb3.append("	    }	");
		sb3.append("	  },	");
		sb3.append("	  \"source\": \"java-webhook\"	");
		sb3.append("	}	");
		sb.append(sb3.toString());
		System.out.println(sb.toString());
		return sb.toString();
	}
}
