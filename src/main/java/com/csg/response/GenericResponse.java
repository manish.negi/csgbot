package com.csg.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class GenericResponse 
{
	String speech="";
	public String genericResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  ]	");
		sb.append("	 }	");
		System.out.println(sb.toString());
		return sb.toString();
	}
}
