package com.csg.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class QuestionsResponse 
{
	String speech="";
	public String questionsResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action, Map<String,Map<String,String>> QuestionAswerMap)
	{
		String speeches=speech.replaceAll("[-+?^/]\"]*", "");
		speech=speeches.replaceAll("\"", "");
		StringBuffer sb = new StringBuffer();
		if("Q3".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
			sb.append("	  \"fulfillmentMessages\": [	");
			sb.append("	    {	");
			sb.append("	      \"text\": {	");
			sb.append("	        \"text\": [\""+speech+"\"]	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  ],	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"type\": \"Chatbot\",	");
			sb.append("	      \"platform\": \"API.AI\",	");
			sb.append("	      \"title\": \"MLIChatBot\",	");
			sb.append("	      \"imageUrl\": \"BOT\",	");
			sb.append("	      \"buttons\": [	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" Graduation\",	");
			sb.append("	          \"postback\": \"UG\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"PostGraduation\",	");
			sb.append("	          \"postback\": \"PG\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        }	");
			sb.append("	      ]	");
			sb.append("	    }	");
			sb.append("	  },	");
			sb.append("	  \"source\": \"java-webhook\"	");
			sb.append("	}	");
		}
		else if("Q4".equalsIgnoreCase(action) && "PG".equalsIgnoreCase(QuestionAswerMap.get(sessionId).get("Q3")+""))
		{
			sb.append("	{	");
			sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
			sb.append("	  \"fulfillmentMessages\": [	");
			sb.append("	    {	");
			sb.append("	      \"text\": {	");
			sb.append("	        \"text\": [\""+speech+"\"]	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  ],	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"type\": \"Chatbot\",	");
			sb.append("	      \"platform\": \"API.AI\",	");
			sb.append("	      \"title\": \"MLIChatBot\",	");
			sb.append("	      \"imageUrl\": \"BOT\",	");
			sb.append("	      \"buttons\": [	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" Management\",	");
			sb.append("	          \"postback\": \"Management\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" Medical\",	");
			sb.append("	          \"postback\": \"Medical\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" MTech/Msc\",	");
			sb.append("	          \"postback\": \"MTech/Msc\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" FilmMaking(2Year)\",	");
			sb.append("	          \"postback\": \"Film Making (2 Year Conservatory filmmaking)\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"PostGraduation\",	");
			sb.append("	          \"postback\": \"PG\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        }	");
			sb.append("	      ]	");
			sb.append("	    }	");
			sb.append("	  },	");
			sb.append("	  \"source\": \"java-webhook\"	");
			sb.append("	}	");
		}
		else if("Q4".equalsIgnoreCase(action) && "UG".equalsIgnoreCase(QuestionAswerMap.get(sessionId).get("Q3")+""))
		{
			sb.append("	{	");
			sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
			sb.append("	  \"fulfillmentMessages\": [	");
			sb.append("	    {	");
			sb.append("	      \"text\": {	");
			sb.append("	        \"text\": [\""+speech+"\"]	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  ],	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"type\": \"Chatbot\",	");
			sb.append("	      \"platform\": \"API.AI\",	");
			sb.append("	      \"title\": \"MLIChatBot\",	");
			sb.append("	      \"imageUrl\": \"BOT\",	");
			sb.append("	      \"buttons\": [	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" Arts\",	");
			sb.append("	          \"postback\": \"Arts (3 years)\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" Science\",	");
			sb.append("	          \"postback\": \"Science\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Commerce\",	");
			sb.append("	          \"postback\": \"Commerce\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Law\",	");
			sb.append("	          \"postback\": \"Law (5 years)\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Engineering\",	");
			sb.append("	          \"postback\": \"Engineering\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Fashion\",	");
			sb.append("	          \"postback\": \"Fashion\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Medical\",	");
			sb.append("	          \"postback\": \"Medical (4 years)\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        }	");
			sb.append("	      ]	");
			sb.append("	    }	");
			sb.append("	  },	");
			sb.append("	  \"source\": \"java-webhook\"	");
			sb.append("	}	");
		}
		else if("Q5".equalsIgnoreCase(action))
		{
			sb.append("	{	");
			sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
			sb.append("	  \"fulfillmentMessages\": [	");
			sb.append("	    {	");
			sb.append("	      \"text\": {	");
			sb.append("	        \"text\": [\""+speech+"\"]	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  ],	");
			sb.append("	  \"payload\": {	");
			sb.append("	    \"facebook\": {	");
			sb.append("	      \"type\": \"Chatbot\",	");
			sb.append("	      \"platform\": \"API.AI\",	");
			sb.append("	      \"title\": \"MLIChatBot\",	");
			sb.append("	      \"imageUrl\": \"BOT\",	");
			sb.append("	      \"buttons\": [	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" India\",	");
			sb.append("	          \"postback\": \"India\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" US\",	");
			sb.append("	          \"postback\": \"US\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \" UK\",	");
			sb.append("	          \"postback\": \"UK\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Australia\",	");
			sb.append("	          \"postback\": \"Australia\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        },	");
			sb.append("	        {	");
			sb.append("	          \"text\": \"Singapore\",	");
			sb.append("	          \"postback\": \"Singapore\",	");
			sb.append("	          \"link\": \"\"	");
			sb.append("	        }	");
			sb.append("	      ]	");
			sb.append("	    }	");
			sb.append("	  },	");
			sb.append("	  \"source\": \"java-webhook\"	");
			sb.append("	}	");
		}
		else
		{
			sb.append("	{	");
			sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
			sb.append("	  \"fulfillmentMessages\": [	");
			sb.append("	    {	");
			sb.append("	      \"text\": {	");
			sb.append("	        \"text\": [\""+speech+"\"]	");
			sb.append("	      }	");
			sb.append("	    }	");
			sb.append("	  ]	");
			sb.append("	 }	");
		}
		System.out.println(sb.toString());
		return sb.toString();
	}
}
