package com.csg.response;

public class InnerContextData 
{
	private String givenPolicyNumber;


	public String getGivenPolicyNumber() {
		return givenPolicyNumber;
	}

	public void setGivenPolicyNumber(String givenPolicyNumber) {
		this.givenPolicyNumber = givenPolicyNumber;
	}
	
}
