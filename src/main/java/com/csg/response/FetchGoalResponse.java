package com.csg.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class FetchGoalResponse 
{
	String speech="";
	public String fetchGoalResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"fulfillmentText\": \"displayed&spoken response\",	");
		sb.append("	  \"fulfillmentMessages\": [	");
		sb.append("	    {	");
		sb.append("	      \"text\": {	");
		sb.append("	        \"text\": [\""+speech+"\"]	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  ],	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"facebook\": {	");
		sb.append("	      \"type\": \"Chatbot\",	");
		sb.append("	      \"platform\": \"API.AI\",	");
		sb.append("	      \"title\": \"MLIChatBot\",	");
		sb.append("	      \"imageUrl\": \"BOT\",	");
		sb.append("	      \"buttons\": [	");

		StringBuffer sb2= new StringBuffer();
		String lifeGoal = ExternalMap.get(sessionId).get("Goals")+"";
		String [] str = lifeGoal.split(",");
		int sizeGoal = str.length;
		int count = 0;
		for(int i=0; i<str.length; i++)
		{

			count++;
			if(sizeGoal!=count){
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+str[i]+"\",	");
				sb2.append("	          \"postback\": \""+str[i]+"\",	");
				sb2.append("	          \"link\": \"\"	");
				sb2.append("	        },	");
			}
			else{
				sb2.append("	        {	");
				sb2.append("	          \"text\": \" "+str[i]+"\",	");
				sb2.append("	          \"postback\": \""+str[i]+"\",	");
				sb2.append("	          \"link\": \"\"	");
				sb2.append("	        }	");
			}
		}
		sb.append(sb2.toString());
		StringBuffer sb3 = new StringBuffer();
		sb3.append("	      ]	");
		sb3.append("	    }	");
		sb3.append("	  },	");
		sb3.append("	  \"source\": \"java-webhook\"	");
		sb3.append("	}	");
		sb.append(sb3.toString());
		System.out.println(sb.toString());
		return sb.toString();
	}
}
