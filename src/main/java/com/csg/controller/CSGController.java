package com.csg.controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.csg.button.InnerData;
import com.csg.calculator.ChildEducation;
import com.csg.calculator.ChildMarriage;
import com.csg.calculator.Protectionofmyfamily;
import com.csg.calculator.RegularSavings;
import com.csg.calculator.Retirement;
import com.csg.ga.response.GAFinancialGoal;
import com.csg.ga.response.GAFinancialGoalQuestions;
import com.csg.ga.response.GASimpleResponse;
import com.csg.model.CSGQuestions;
import com.csg.model.FinancialProduct;
import com.csg.model.UserQuestions;
import com.csg.response.APIResponse;
import com.csg.response.CalculatorResponse;
import com.csg.response.FetchGoalResponse;
import com.csg.response.QuestionsResponse;

@RestController
@RequestMapping("/csg")
public class CSGController 
{

	@Autowired
	private CSGQuestions csgQuestions;
	@Autowired
	private APIResponse apiResponse;
	@Autowired
	private FinancialProduct financialProduct;
	@Autowired
	private CalculatorResponse calculatorResponse;
	@Autowired
	private UserQuestions userQuestions;
	@Autowired
	private QuestionsResponse questionsResponse;
	@Autowired
	private FetchGoalResponse fetchGoalResponse;
	@Autowired
	private Protectionofmyfamily protectionofmyfamily;
	@Autowired
	private RegularSavings regularSavings;
	@Autowired
	private Retirement retirement;
	@Autowired
	private GAFinancialGoalQuestions gaFinancialGoalQuestions;
	@Autowired
	private GAFinancialGoal gaFinancialGoal;
	@Autowired
	private GASimpleResponse gaSimpleResponse;
	@Autowired
	private ChildMarriage childMarriage;
	@Autowired
	private ChildEducation childEducation;

	static Map<String,Map<String,String>> ExternalMap = new ConcurrentHashMap<String,Map<String,String>>();
	static Map<String,Map<String,String>> QuestionAswerMap = new ConcurrentHashMap<String,Map<String,String>>();
	Map<String,String> captureSource = new ConcurrentHashMap<String,String>();
	Map<String,String> QuestionsMap = new LinkedHashMap<String,String>();
	Map<String,Map<String,String>> response;

	@RequestMapping(method = RequestMethod.POST)
	public String webhook(@RequestBody String obj) 
	{
		System.out.println("Testing :-"+obj);
		InnerData innerData = new InnerData();
		System.out.println("Inside Controller");
		System.out.println(obj.toString());
		String speech="", sessionId = "", action="", customerName="";
		String questions="", custEmail="", mobileNum="", date="", smoke="", source="";
		try {
			JSONObject object = new JSONObject(obj.toString());
			System.out.println("Request	 "+obj.toString());
			sessionId = object.get("session")+"";
			System.out.println("SessionId--"+sessionId);
			action = object.getJSONObject("queryResult").get("action") + "";
			try {
				source = object.getJSONObject("originalDetectIntentRequest").get("source") + "";
				captureSource.put(sessionId, source);
			}
			catch(Exception ex){
				source="normal";
				captureSource.put(sessionId, source);
			}
			System.out.println("-----------"+action);
			try {
				questions = object.getJSONObject("queryResult").get("queryText")+"";
				if("proceed.proceed-custom".equalsIgnoreCase(action))
				{
					if(ExternalMap.containsKey(sessionId))
					{
						QuestionsMap=QuestionAswerMap.get(sessionId);
						if(QuestionsMap!=null)
						{
							for(Entry<String,String> entry : QuestionsMap.entrySet())
							{
								String key =entry.getKey();
								String value =entry.getValue();
								if("".equalsIgnoreCase(value))
								{
									String []strSplit = questions.split("\\$\\$");
									QuestionsMap.put(key, strSplit[1]);
									QuestionAswerMap.put(sessionId, QuestionsMap);
								}
							}
						}
					}
				}
				System.out.println("---Questions1--------"+questions);
			}
			catch(Exception ex)
			{
				questions="";
			}
			System.out.println(action.toUpperCase());
			switch(action.toUpperCase())
			{
			case "INPUT.WORRY":
			case "GA.WELCOME":
			{
				if("GA.WELCOME".equalsIgnoreCase(action)){
					speech=gaFinancialGoalQuestions.gaFinancialGoalQuestions(ExternalMap, sessionId);
				}
				else{
					action="Worry";
					speech=csgQuestions.csgQuestions(ExternalMap, sessionId, action);
					speech=apiResponse.apiResponse(ExternalMap, sessionId, speech, action);
				}

			}
			break;
			/*case "INPUT.HAPINESS":
			{
				action="Happiness";
				speech=csgQuestions.csgQuestions(ExternalMap, sessionId, action);
				speech=apiResponse.apiResponse(ExternalMap, sessionId, speech, action);
			}
			break;*/
			case "INPUT.PROCEED":
			case "INPUT.UNKNOWN":
			{
				if("INPUT.UNKNOWN".equalsIgnoreCase(action)){
					System.out.println("--------"+action);
					speech=gaFinancialGoal.financialGoal();
				}
				else{
					action="Proceed";
					speech=financialProduct.financialProduct(ExternalMap, sessionId, action, questions);
					speech=fetchGoalResponse.fetchGoalResponse(ExternalMap, sessionId, speech, action);
				}
			}
			break;
			case "PROCEED.PROCEED-CUSTOM":
			{
				action="Questions";
				if(!"Exists".equalsIgnoreCase(ExternalMap.get(sessionId).get("Identifier")+""))
				{
					if("google".equalsIgnoreCase(captureSource.get(sessionId)+""))
					{
						questions="Protection";
						response=userQuestions.userQuestions(ExternalMap, sessionId, action, questions);
					}
					else{
						response=userQuestions.userQuestions(ExternalMap, sessionId, action, questions);
					}
					LinkedHashMap<String,String> linkedQuestions=(LinkedHashMap<String, String>) response.get(sessionId);
					for (Entry<String, String> entry : linkedQuestions.entrySet()) {
						String key = entry.getKey();
						String value = entry.getValue();
						QuestionsMap=new LinkedHashMap<String,String>();
						QuestionsMap.put(key, "");
						QuestionAswerMap.put(sessionId, QuestionsMap);
						if("google".equalsIgnoreCase(captureSource.get(sessionId)+"")){
							speech=gaSimpleResponse.simpleResponse(ExternalMap, sessionId, value, key);
						}
						else{
							speech=questionsResponse.questionsResponse(ExternalMap, sessionId, value, key, QuestionAswerMap);
						}
						response.get(sessionId).remove(key);
						break;
					}
				}
				else
				{
					LinkedHashMap<String,String> linkedQuestions=(LinkedHashMap<String, String>) response.get(sessionId);
					if(!linkedQuestions.isEmpty())
					{
						for (Entry<String, String> entry : linkedQuestions.entrySet()) {
							String key = entry.getKey();
							String value = entry.getValue();
							QuestionsMap.put(key, "");
							QuestionAswerMap.put(sessionId, QuestionsMap);
							if("google".equalsIgnoreCase(captureSource.get(sessionId)+"")){
								speech=gaSimpleResponse.simpleResponse(ExternalMap, sessionId, value, key);
							}
							else{
								speech=questionsResponse.questionsResponse(ExternalMap, sessionId, value, key, QuestionAswerMap);
							}
							response.get(sessionId).remove(key);
							break;
						}
					}
					else
					{
						//Call to Calculator
						System.out.println("------Call to Calculator");
						String responsefromCalc=ExternalMap.get(sessionId).get("Goals");
						String [] splitResponse = responsefromCalc.split(",");
						for(int i=0; i<splitResponse.length;i++)
						{
							if("PF".equalsIgnoreCase(splitResponse[i]))
							{
								String responseprotectionofmyfamily=protectionofmyfamily.protectionOfMyFamily(QuestionAswerMap, sessionId);
								ExternalMap.get(sessionId).put("protectionofmyfamily", responseprotectionofmyfamily);
							}
							else if("RS".equalsIgnoreCase(splitResponse[i]))
							{
								String responseRegularSaving=regularSavings.regularSavings(QuestionAswerMap, sessionId);
								ExternalMap.get(sessionId).put("RegularSaving", responseRegularSaving);
							}
							else if("ER".equalsIgnoreCase(splitResponse[i]))
							{
								String responseRetirement=retirement.retirement(QuestionAswerMap, sessionId);	
								ExternalMap.get(sessionId).put("Retirement", responseRetirement);
							}
							else if("CE".equalsIgnoreCase(splitResponse[i]))
							{
								String responsechildEducation=childEducation.childEducation(QuestionAswerMap, sessionId);	
								ExternalMap.get(sessionId).put("ChildEducation", responsechildEducation);
							}
							else if("CM".equalsIgnoreCase(splitResponse[i]))
							{
								String responseChildMarriage=childMarriage.childMarriage(QuestionAswerMap, sessionId);	
								ExternalMap.get(sessionId).put("ChildMarriage", responseChildMarriage);
							}
						}
						speech=calculatorResponse.calculatorResponse(ExternalMap, sessionId, speech, action);
					}
				}
			}
			break;
			default : 
			{
				System.out.println("Intent Not match with skill, Please connect to application owner");
				speech="Intent Not match with skill, Please connect to application owner";
			}
			}
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured");
			speech="Communication glitch while calling API's.";
		}
		System.out.println("Final Speech--"+speech);
		return speech;
	}

}
