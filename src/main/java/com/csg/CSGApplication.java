package com.csg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CSGApplication {

	public static void main(String[] args) {
		SpringApplication.run(CSGApplication.class, args);
	}
}
