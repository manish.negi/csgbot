package com.csg.ga.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class GAFinancialGoalQuestions 
{
	public String gaFinancialGoalQuestions(Map<String,Map<String,String>> ExternalMap, String sessionId)
	{
		//this map Only for testing purpose
		Map<String,String> map = new HashMap<String,String> ();
		map.put("1", "1");
		ExternalMap.put(sessionId, map);
		String speech="Please select a Questions";
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"google\": {	");
		sb.append("	      \"expectUserResponse\": true,	");
		sb.append("	      \"richResponse\": {	");
		sb.append("	        \"items\": [	");
		sb.append("	          {	");
		sb.append("	            \"simpleResponse\": {	");
		sb.append("	              \"textToSpeech\": \""+speech+"\"	");
		sb.append("	            }	");
		sb.append("	          }	");
		sb.append("	        ]	");
		sb.append("	      },	");
		sb.append("	      \"systemIntent\": {	");
		sb.append("	        \"intent\": \"actions.intent.OPTION\",	");
		sb.append("	        \"data\": {	");
		sb.append("	          \"@type\": \"type.googleapis.com/google.actions.v2.OptionValueSpec\",	");
		sb.append("	          \"listSelect\": {	");
		sb.append("	            \"title\": \"Questions\",	");
		sb.append("	            \"items\": [	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W1\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"\",	");
		sb.append("	                  \"accessibilityText\": \"first alt\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"If something were to happen to me-how will my family keep up with financial needs?\"	");
		sb.append("	              },	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W2\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"\",	");
		sb.append("	                  \"accessibilityText\": \"first alt\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"What if a disease or emergency takes up significant portion of my income?\"	");
		sb.append("	              },	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W3\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"\",	");
		sb.append("	                  \"accessibilityText\": \"first alt\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"Will I be able to afford good education for my child?\"	");
		sb.append("	              },	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W4\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"\",	");
		sb.append("	                  \"accessibilityText\": \"first alt\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"Will I be able to afford a good wedding for my children?\"	");
		sb.append("	              },	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W5\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"\",	");
		sb.append("	                  \"accessibilityText\": \"first alt\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"How do I ensure my savings & investments grow at a steady rate?\"	");
		sb.append("	              },	");
		sb.append("	              {	");
		sb.append("	                \"optionInfo\": {	");
		sb.append("	                  \"key\": \"W6\"	");
		sb.append("	                },	");
		sb.append("	                \"description\": \"\",	");
		sb.append("	                \"image\": {	");
		sb.append("	                  \"url\": \"https://lh3.googleusercontent.com/Nu3a6F80WfixUqf_ec_vgXy_c0-0r4VLJRXjVFF_X_CIilEu8B9fT35qyTEj_PEsKw\",	");
		sb.append("	                  \"accessibilityText\": \"secondtext\"	");
		sb.append("	                },	");
		sb.append("	                \"title\": \"Will I be able to save up enough for my life post retirement?\"	");
		sb.append("	              }	");
		sb.append("	            ]	");
		sb.append("	          }	");
		sb.append("	        }	");
		sb.append("	      }	");
		sb.append("	    }	");
		sb.append("	  }	");
		sb.append("	}	");
		System.out.println("-----------------testing"+sb.toString());
		return sb.toString();
	}

}
