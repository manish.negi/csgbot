package com.csg.ga.response;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class GASimpleResponse 
{
	public String simpleResponse(Map<String,Map<String,String>> ExternalMap, String sessionId, String speech, String action)
	{
		String speeches=speech.replaceAll("[-+?^/]\"]*", "");
		speech=speeches.replaceAll("\"", "");
		StringBuilder requestdata = new StringBuilder();
		requestdata.append("	{	");
		requestdata.append("	  \"payload\": {	");
		requestdata.append("	    \"google\": {	");
		requestdata.append("	      \"expectUserResponse\": true,	");
		requestdata.append("	      \"richResponse\": {	");
		requestdata.append("	        \"items\": [	");
		requestdata.append("	          {	");
		requestdata.append("	            \"simpleResponse\": {	");
		requestdata.append("	              \"textToSpeech\": \""+speech+"\"	");
		requestdata.append("	            }	");
		requestdata.append("	          }	");
		requestdata.append("	        ]	");
		requestdata.append("	      }	");
		requestdata.append("	    },	");
		requestdata.append("	    \"facebook\": {	");
		requestdata.append("	      \"text\": \"Hello, Facebook!\"	");
		requestdata.append("	    },	");
		requestdata.append("	    \"slack\": {	");
		requestdata.append("	      \"text\": \"This is a text response for Slack.\"	");
		requestdata.append("	    }	");
		requestdata.append("	  }	");
		requestdata.append("	}	");
		return requestdata.toString();
	}

}
