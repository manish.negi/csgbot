package com.csg.ga.response;

import org.springframework.stereotype.Service;

@Service
public class GAFinancialGoal 
{
	public String financialGoal()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("	{	");
		sb.append("	  \"payload\": {	");
		sb.append("	    \"google\": {	");
		sb.append("	      \"expectUserResponse\": true,	");
		sb.append("	      \"richResponse\": {	");
		sb.append("	        \"items\": [	");
		sb.append("	          {	");
		sb.append("	            \"simpleResponse\": {	");
		sb.append("	              \"textToSpeech\": \"Based on your selection, we have your TOP financial goals ready-\"	");
		sb.append("	            }	");
		sb.append("	          },	");
		sb.append("	                        {	");
		sb.append("	                            \"basicCard\": {	");
		sb.append("	                                \"title\": \"Financial Goal\",	");
		sb.append("	                                \"formattedText\": \"Once you have finalized your TOP goals, lets move to determine the capital you need to fulfill them-\",	");
		sb.append("	                                \"image\": {	");
		sb.append("	                                    \"url\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSNMuk6UhqeooJx1rqjI2vQuIkUEFoWx4Xo4GihaMiksepMoqyo\",	");
		sb.append("	                                    \"accessibilityText\": \"Image alternate text\"	");
		sb.append("	                                },	");
		sb.append("	                                \"buttons\": [	");
		sb.append("	                                    {	");
		sb.append("	                                        \"title\": \"Click Proceed Button\",	");
		sb.append("	                                        \"openUrlAction\": {	");
		sb.append("	                                            \"url\": \"https://pay.google.com/payments/u/0/home\"	");
		sb.append("	                                        }	");
		sb.append("	                                    }	");
		sb.append("	                                ],	");
		sb.append("	                                \"imageDisplayOptions\": \"CROPPED\"	");
		sb.append("	                            }	");
		sb.append("	                        }	");
		sb.append("	        ],	");
		sb.append("	                    \"suggestions\": [	");
		sb.append("	                        {	");
		sb.append("	                            \"title\": \"answer abc\"	");
		sb.append("	                        }	");
		sb.append("	                    ]	");
		sb.append("	      }	");
		sb.append("	    },	");
		sb.append("	    \"facebook\": {	");
		sb.append("	      \"text\": \"Hello, Facebook!\"	");
		sb.append("	    },	");
		sb.append("	    \"slack\": {	");
		sb.append("	      \"text\": \"This is a text response for Slack...\"	");
		sb.append("	    }	");
		sb.append("	  }	");
		sb.append("	}	");
		return sb.toString();
	}
}
