package com.csg.model;

import java.util.Map;

public interface UserQuestions 
{
	public Map<String,Map<String,String>> userQuestions(Map<String, Map<String, String>> ExternalMap, String sessionId, String action, String questions); 

}
