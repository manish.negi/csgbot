package com.csg.modelImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.model.UserQuestions;
import com.csg.service.UserQuestionsCall;

@Service
public class UserQuestionsImpl implements UserQuestions
{
	@Autowired
	private UserQuestionsCall userQuestionsCall;
	static Map<String,Map<String,String>> QuestionMap = new ConcurrentHashMap<String,Map<String,String>>();
	@Override
	public Map<String,Map<String,String>> userQuestions(Map<String, Map<String, String>> ExternalMap, String sessionId, String action, String questionsId) 
	{
		StringBuffer sb = new StringBuffer();
		Map<String,String> map = new LinkedHashMap<String,String>();
		//String Goal = ExternalMap.get(sessionId).get("Goals");
		String [] goalSplit = questionsId.split("\\$\\$");
		for(int i=0; i<goalSplit.length; i++)
		{
			action = goalSplit[i];
			switch(action)
			{
			case "Protection":
			{
				action="PF";
			}
			break;
			case "Savings/Wealth":
			{
				action="RS";
			}
			break;
			case "Child Education":
			{
				action="CE";
			}
			break;
			case "Legacy":
			{
				action="LG";
			}
			break;
			case "Retirement":
			{
				action="ER";
			}
			break;
			case "Child Marriage":
			{
				action="CM";
			}
			break;
			default:
			{
				action="default";
			}
			}
			if(!"default".equalsIgnoreCase(action))
			{
				sb.append(action+",");
				String questions = userQuestionsCall.userQuestionsCall(ExternalMap, sessionId, action, questionsId);
				String questionsArray[] = questions.split(",");
				for(int j=0; j<questionsArray.length; j++)
				{
					String str=questionsArray[j];
					String strArray[] =str.split("\\$");
					map.put(strArray[1], strArray[2]);
				}
				System.out.println(questions);
				QuestionMap.put(sessionId, map);
				ExternalMap.get(sessionId).put("Identifier", "Exists");
			}
		}
		ExternalMap.get(sessionId).put("Goals", sb.toString());
		return QuestionMap;
	}
}
