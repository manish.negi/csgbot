package com.csg.modelImpl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.model.CSGQuestions;
import com.csg.service.CSGQuestionsCall;



@Service
public class CSGQuestionsImpl implements CSGQuestions
{
	@Autowired
	private CSGQuestionsCall csgQuestionCall;
	String speech="";
	@Override
	public String csgQuestions(Map<String,Map<String,String>> ExternalMap, String sessionId, String action) 
	{
		Map<String,String> internalMap = new LinkedHashMap<String,String>();
		String questionResponse=csgQuestionCall.csgQuestionsServiceCall(ExternalMap, sessionId, action);
		JSONArray jsonArr = new JSONArray(questionResponse);
		for(int i=0; i<jsonArr.length();i++)
		{
			String result = jsonArr.get(i)+"";
			String [] arr  = result.split(",");
			internalMap.put(arr[0], arr[1]);
		}
		
		ExternalMap.put(sessionId, internalMap);
		speech="Thanks, Please choose your Life's BIG goals from the list below as per your priority-";
		return speech;
	}
}
