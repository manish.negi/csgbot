package com.csg.modelImpl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.csg.model.FinancialProduct;
import com.csg.service.FinancialProductCall;

@Service
public class FinancialProductImpl implements FinancialProduct 
{
	@Autowired
	private FinancialProductCall financialProductCall;
	
	@Override
	public String financialProduct(Map<String, Map<String, String>> ExternalMap, String sessionId, String action, String questions) {
		Map<String,String> map = new HashMap<String,String>();
		
		String goalResult = financialProductCall.financialProductCall(ExternalMap, sessionId, action, questions);
		map.put("Goals", goalResult);
		ExternalMap.put(sessionId, map);
		goalResult="Based on your selection we have your TOP financial goals ready $ Once you have fanalized "
				+ "your TOP goals, lets move on to determine the capital you need to fulfill them $ Shall we proceed";
		return goalResult;
	}

}
