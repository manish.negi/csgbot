package com.csg.modelImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.model.CSGWebServiceCall;
import com.csg.service.CSGQuestionsCall;

@Service
public class CSGWebServiceCallImpl implements CSGWebServiceCall 
{
	@Autowired
	private CSGQuestionsCall csgQuestionsCall;
	
	@Override
	public String csgWebServiceCall() {
		/*CSG Web Service third party call to get name n all*/
		return null;
	}

}
