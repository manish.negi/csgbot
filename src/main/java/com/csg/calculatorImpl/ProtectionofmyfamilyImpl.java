package com.csg.calculatorImpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.calculator.Protectionofmyfamily;
import com.csg.service.ProtectionofmyfamilyCalc;

@Service
public class ProtectionofmyfamilyImpl implements Protectionofmyfamily 
{
	@Autowired
	private ProtectionofmyfamilyCalc protectionOfMyFamilyCalc;

	@Override
	public String protectionOfMyFamily(Map<String, Map<String, String>> QuestionAswerMap, String sessionId)
	{
		String age="", monthlyExp="", savingInvest="", loans="", futureGoal="", existingCover="";

		for(Entry<String,String> entry : QuestionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q13".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q15".equalsIgnoreCase(key))
			{
				monthlyExp=value;
			}
			else if("Q16".equalsIgnoreCase(key))
			{
				savingInvest=value;
			}
			else if("Q17".equalsIgnoreCase(key))
			{
				loans=value;
			}
			else if("Q14".equalsIgnoreCase(key))
			{
				futureGoal=value;
			}
			else if("Q18".equalsIgnoreCase(key))
			{
				existingCover=value;
			}
		}
		String protecttoMyFamily = protectionOfMyFamilyCalc.protectionOfMyFamilyCalculation(age, monthlyExp, savingInvest, loans, futureGoal, existingCover);
		return protecttoMyFamily;
	}
}
