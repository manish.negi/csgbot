package com.csg.calculatorImpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.calculator.ChildMarriage;
import com.csg.service.ChildMarriageCalculator;

@Service
public class ChildMarriageImpl implements ChildMarriage 
{
	@Autowired
	private ChildMarriageCalculator childMarriageCalculator; 
	@Override
	public String childMarriage(Map<String, Map<String, String>> QuestionAswerMap, String sessionId) 
	{
		String age="", marriageAge="", neededMoney="", asideMoney="", ExpectSaving="";

		for(Entry<String,String> entry : QuestionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q2".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q10".equalsIgnoreCase(key))
			{
				marriageAge=value;
			}
			else if("Q11".equalsIgnoreCase(key))
			{
				neededMoney=value;
			}
			else if("Q12".equalsIgnoreCase(key))
			{
				asideMoney=value;
			}
			else if("Q9".equalsIgnoreCase(key))
			{
				ExpectSaving=value;
				if("N".equalsIgnoreCase(ExpectSaving))
				{
					ExpectSaving="8";
				}
			}
		}
		String childMarriage=childMarriageCalculator.childMarriageCalculator(age, marriageAge, neededMoney, asideMoney, ExpectSaving);
		return childMarriage;
	}
}
