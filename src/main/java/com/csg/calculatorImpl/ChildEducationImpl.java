package com.csg.calculatorImpl;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csg.calculator.ChildEducation;
import com.csg.service.ChildEducationCalculator;

@Service
public class ChildEducationImpl implements ChildEducation 
{
	@Autowired
	private ChildEducationCalculator childEducationCalculator;

	@Override
	public String childEducation(Map<String, Map<String, String>> QuestionAswerMap, String sessionId)
	{
		String age="", graduation="", studyType="", Country="", currentSavings="", childAge="", 
				inflationRate="", rateReturnInvestment="";
		for(Entry<String,String> entry : QuestionAswerMap.get(sessionId).entrySet())
		{
			String key =entry.getKey();
			String value =entry.getValue();
			if("Q2".equalsIgnoreCase(key))
			{
				age=value;
			}
			else if("Q3".equalsIgnoreCase(key))
			{
				graduation=value;
			}
			else if("Q4".equalsIgnoreCase(key))
			{
				studyType=value;
			}
			else if("Q5".equalsIgnoreCase(key))
			{
				Country=value;
			}
			else if("Q6".equalsIgnoreCase(key))
			{
				currentSavings=value;
			}
			else if("Q7".equalsIgnoreCase(key))
			{
				childAge=value;
			}
			else if("Q8".equalsIgnoreCase(key))
			{
				inflationRate=value;
				if("N".equalsIgnoreCase(inflationRate))
				{
					inflationRate="8";
				}
			}
			else if("Q9".equalsIgnoreCase(key))
			{
				rateReturnInvestment=value;
				if("N".equalsIgnoreCase(rateReturnInvestment))
				{
					rateReturnInvestment="8";
				}
				
			}
		}
		String responseChildEducation=childEducationCalculator.childEducationCalculator(age, graduation, studyType, Country, currentSavings, 
				childAge,inflationRate, rateReturnInvestment);
		return responseChildEducation;
	}

}
