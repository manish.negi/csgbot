package com.csg.serviceImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.csg.service.RetirementCalc;

@Service
public class RetirementCalcImpl implements RetirementCalc 
{
	String responseRetiremnet="";
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getRetirementPremium";

	@Override
	public String retirementCalc(String age, String retirePlan, String monthlyExp, String expAfterRetire, 
			String inflationRate, String saving, String expectedRetSaving) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"retirePlan\":\""+retirePlan+"\",	");
		sb.append("	  \"monthlyExp\":\""+monthlyExp+"\",	");
		sb.append("	  \"expAfterRetire\":\""+expAfterRetire+"\",	");
		sb.append("	  \"inflationRate\":\""+inflationRate+"\",	");
		sb.append("	  \"saving\":\""+saving+"\",	");
		sb.append("	  \"expectedRetSaving\":\""+expectedRetSaving+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			responseRetiremnet = response.getBody();
			System.out.println("--------"+responseRetiremnet);
		}
		return responseRetiremnet;
	}
}