package com.csg.serviceImpl;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.csg.service.FinancialProductCall;

@Service
public class FinancialProductCallImpl implements FinancialProductCall 
{
	String finalProduct="";
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getUserFinancialGoals";
	@Override
	public String financialProductCall(Map<String, Map<String, String>> ExternalMap, String sessionId, String action, String questions) {
		String [] fact = questions.split(" ");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"fact1\":\""+fact[1]+"\",");
		sb.append("	  \"fact2\":\""+fact[2]+"\",");
		sb.append("	  \"fact3\":\""+fact[3]+"\"");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			finalProduct = response.getBody();
			System.out.println("--------"+finalProduct);

		}
		return finalProduct;
	}

}
