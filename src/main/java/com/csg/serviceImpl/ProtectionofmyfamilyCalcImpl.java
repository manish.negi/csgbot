package com.csg.serviceImpl;

import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.csg.service.ProtectionofmyfamilyCalc;

@Service
public class ProtectionofmyfamilyCalcImpl implements ProtectionofmyfamilyCalc
{
	String resultProtectToMyFamily="";
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getProtectionPremium";
	@Override
	public String protectionOfMyFamilyCalculation(String age, String monthlyExp, String savingInvest, String loans, String futureGoal, String existingCover)
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"monthlyExp\":\""+monthlyExp+"\",	");
		sb.append("	  \"savingInvest\":\""+savingInvest+"\",	");
		sb.append("	  \"loans\":\""+loans+"\",	");
		sb.append("	  \"futureGoal\":\""+futureGoal+"\",	");
		sb.append("	  \"existingCover\":\""+existingCover+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultProtectToMyFamily = response.getBody();
			System.out.println("--------"+resultProtectToMyFamily);
		}
		return resultProtectToMyFamily;
	}
}