package com.csg.serviceImpl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.csg.service.ChildMarriageCalculator;

@Service
public class ChildMarriageCalculatorImpl implements ChildMarriageCalculator 
{
	String resultChildMarriage;
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getChildMarriagePremium";
	@Override
	public String childMarriageCalculator(String age, String marriageAge, String neededMoney, String asideMoney, 
			String ExpectSaving) 
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"marriageAge\":\""+marriageAge+"\",	");
		sb.append("	  \"neededMoney\":\""+neededMoney+"\",	");
		sb.append("	  \"asideMoney\":\""+asideMoney+"\",	");
		sb.append("	  \"ExpectSaving\":\""+ExpectSaving+"\"	");
		sb.append("	}	");
		System.out.println("Child Marriage Calculator call to webstudio:--"+sb.toString());
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultChildMarriage = response.getBody();
			System.out.println("--------"+resultChildMarriage);
		}
		return resultChildMarriage;
	}
}
