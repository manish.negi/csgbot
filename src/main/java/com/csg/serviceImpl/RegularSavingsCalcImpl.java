package com.csg.serviceImpl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.csg.service.RegularSavingsCalc;

@Service
public class RegularSavingsCalcImpl implements RegularSavingsCalc 
{
	String resultRegularSavings="";
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getSavingPremium";
	@Override
	public String regularSavingCalculation(String age, String goalAge, String currentCostOfGoal, String currentProvision,
			String inflationRate, String rateReturnInvestment)
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"goalAge\":\""+goalAge+"\",	");
		sb.append("	  \"currentCostOfGoal\":\""+currentCostOfGoal+"\",");
		sb.append("	  \"currentProvision\":\""+currentProvision+"\",");
		sb.append("	  \"inflationRate\":\""+inflationRate+"\",");
		sb.append("	  \"rateReturnInvestment\":\""+rateReturnInvestment+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultRegularSavings = response.getBody();
			System.out.println("--------"+resultRegularSavings);
		}
		return resultRegularSavings;
	}
}