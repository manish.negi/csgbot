package com.csg.serviceImpl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.csg.service.ChildEducationCalculator;

@Service
public class ChildEducationCalculatorImpl implements ChildEducationCalculator  
{
	String resultChildEducation;
	String url="https://botskilluat.maxlifeinsurance.com/webservice/REST/NeedAnalysisBOT/NeedAnalysisBOT/getChildEduationPremium";
	@Override
	public String childEducationCalculator(String age, String graduation, String studyType, String Country, String currentSavings, 
			String childAge,String inflationRate, String rateReturnInvestment) 
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append("	{	");
		sb.append("	  \"age\":\""+age+"\",	");
		sb.append("	  \"graduation\":\""+graduation+"\",	");
		sb.append("	  \"studyType\":\""+studyType+"\",	");
		sb.append("	  \"Country\":\""+Country+"\",	");
		sb.append("	  \"currentSavings\":\""+currentSavings+"\",	");
		sb.append("	  \"childAge\":\""+childAge+"\",	");
		sb.append("	  \"inflationRate\":\""+inflationRate+"\",	");
		sb.append("	  \"rateReturnInvestment\":\""+rateReturnInvestment+"\"	");
		sb.append("	}	");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			resultChildEducation = response.getBody();
			System.out.println("--------"+resultChildEducation);
		}
		return resultChildEducation;
	}

}
